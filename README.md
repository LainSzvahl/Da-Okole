This is a mod pack for my server Da Okole - a 7 days to die server

Modlets and other changes in this pack is is specific to my server
and i will try to keep up with updates of the mods on the client
and server.

All mods,plugins are owned and maintained by their respective owners
a list of mods running on this server are listed below with author's name.
All mods found here can be found on the official forums @ https://7daystodie.com/forums

===========================================================================================

Telrics Health Bars
JaxTeller718 - CarRespawner
JaxTeller718 - Birds Nest And Trash Destroy On Loot
JaxTeller718 - Bigger Wandering Hordes
MeanCloud - Enemy Reach Shortener v1.01
MeanCloud - Increased Animals v1.00
Malacay2k11 - New Life Experience 1.9.6 ALPHA18 - Hotfixes and Changes-171-1-9-6-1573296637
oignonchaud - Hitmarkers
Guppycur/Ragsy2145 - Boating Modlet A18 v3
Sythalin - Speed Perk
Sythalin - x2 Torches/Candles
Sythalin - Armor Mobility Rework
Stasis - Farm Life mod 2 - updated by MeanCloud for a18 compat
Stasis and syn7572 - Farm Life Mod icons
Stasis and syn7572 - Farm Life Mod Models
Stallions Dens - Pets